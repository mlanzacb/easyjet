#include "../BaselineVarsXbbCalibAlg.h"
#include "../BaselineVarsZbbjCalibAlg.h"
#include "../XbbCalibSelectorAlg.h"
#include "../ZbbjCalibSelectorAlg.h"

using namespace XBBCALIB;

DECLARE_COMPONENT(BaselineVarsXbbCalibAlg)
DECLARE_COMPONENT(BaselineVarsZbbjCalibAlg)
DECLARE_COMPONENT(XbbCalibSelectorAlg)
DECLARE_COMPONENT(ZbbjCalibSelectorAlg)
